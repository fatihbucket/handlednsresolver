/*
 * AUTHOR FATIH BERBER 
 * EMAIL fatih.berber@gwdg.de
 * DATE 2017/04/06
 * 
 * */
package dns.gwdg.de;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.xbill.DNS.Lookup;
import org.xbill.DNS.Record;
import org.xbill.DNS.TXTRecord;
import org.xbill.DNS.TextParseException;
import org.xbill.DNS.Type;

public class DnsHandleResolver {
	private final String DNS_PATTERN = "[a-zA-Z0-9]+\\.[a-zA-Z0-9]+(\\.[a-zA-Z0-9]+(\\.[a-zA-Z0-9]+)?)";
	private final String  HANDLE_CONFIG_PATTERN = "(\\s)*options(\\s)+handlezone(\\s)+(.)*"+DNS_PATTERN;
	private final String  RESOLVER_CONFIG = "/etc/dns-handle-resolv.conf";
	private Pattern pattern = Pattern.compile(DNS_PATTERN);
	private String HANDLE_DNS_ZONE = "hx.gwdg.de";
	
	public DnsHandleResolver(){
		
	}
	
	
	public String getTypeValueByName(String handle,String type){
		return getTypesByName(handle,type);
	}
	
	public String getTypesByName(String handle){
		return getTypesByName(handle,null);
	}
	
	private String getTypesByName(String handle, String type){
		
		if (HANDLE_DNS_ZONE==null){
			System.err.println("ERROR: could not find handlzone in /etc/resolv.conf");
			return "{\"error\":\"handlezone not defined in /etc/resolv.conf\"}";
		}
				
		String jsonResponse = null;
		Record[] records;
		try {
			String dnsHandle = convertHandleToDomainName(handle);
			records = new Lookup(dnsHandle, Type.TXT).run();
			
			StringBuilder sb = new StringBuilder("{");
			for (int i = 0; i < records.length; i++) {
				TXTRecord txt = (TXTRecord) records[i];
				String typeAndData = txt.rdataToString();
				typeAndData = typeAndData.substring(1, typeAndData.length()-1);
				String [] splt_type_data = typeAndData.split("=");
				String theType = splt_type_data[0];
				String theData = splt_type_data[1];
				if (type != null){
					if (theType.equals(type)){
						sb.append("\""+theType+"\":\""+theData+"\",");
					}
				}
			}
			sb.deleteCharAt(sb.length()-1);
			sb.append("}");
			jsonResponse = sb.toString();
			
		} catch (TextParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return jsonResponse;
		
		
	}
	
	
	private String convertHandleToDomainName(String handle){
		
		String [] splt_prefix_suffix = handle.split("/");
		String prefix = splt_prefix_suffix[0];
		String suffix = splt_prefix_suffix[1];
		if(prefix.contains(".")){
			String [] splt_prefix = prefix.split("\\.");
			StringBuilder sb_prefix = new StringBuilder();
			
			for(int j=splt_prefix.length-1; j>= 0; j--){
				sb_prefix.append(splt_prefix[j]);
				sb_prefix.append(".");
			}
			
			prefix = sb_prefix.toString();
		
		}
		
		StringBuilder sb = new StringBuilder(suffix+".-.");
		sb.append(prefix);
		sb.append(HANDLE_DNS_ZONE);
		
		return sb.toString();
	}
	
	

}
